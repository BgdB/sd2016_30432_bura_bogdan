package gr30432.assign1.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import gr30432.assign1.dao.UserDao;
import gr30432.assign1.model.User;


@Repository
public class UserDao {
	@Autowired
	private SessionFactory session;
	
	public void add(User user) {
		session.getCurrentSession().save(user);
	}

	public void edit(User user) {
		session.getCurrentSession().update(user);
	}

	public void delete(int id) {		
		session.getCurrentSession().delete(getUser(id));
	}

	public User getUser(int id) {
		return (User)session.getCurrentSession().get(User.class, id);
	}

	public List getAllUsers() {
		return session.getCurrentSession().createQuery("from User").list();
	}

}
