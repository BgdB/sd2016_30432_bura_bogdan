package gr30432.assign1.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gr30432.assign1.model.User;
import gr30432.assign1.dao.UserDao;

@Service
public class UserService {
	@Autowired
	private UserDao userDao;
	
	@Transactional
	public void add(User student) {
		userDao.add(student);
	}

	@Transactional
	public void edit(User student) {
		userDao.edit(student);
	}

	@Transactional
	public void delete(int id) {
		userDao.delete(id);
	}

	@Transactional
	public User getUser(int id) {
		return userDao.getUser(id);
	}

	@Transactional
	public List getAllUsers() {
		return userDao.getAllUsers();
	}

}

