package gr30432.assign1.service;

import org.springframework.beans.factory.annotation.Autowired;

import gr30432.assign1.dao.LoginDao;

public class LoginService {

	 @Autowired
	 private LoginDao loginDAO;
	 
	 public boolean checkLogin(String username, String password){
         return loginDAO.checkLogin(username, password);
  }
}
