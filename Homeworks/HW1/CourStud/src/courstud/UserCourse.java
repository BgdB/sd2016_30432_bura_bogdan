/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package courstud;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bogdan
 */
@Entity
@Table(name = "user_course")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserCourse.findAll", query = "SELECT u FROM UserCourse u"),
    @NamedQuery(name = "UserCourse.findById", query = "SELECT u FROM UserCourse u WHERE u.id = :id"),
    @NamedQuery(name = "UserCourse.findByStudId", query = "SELECT u FROM UserCourse u WHERE u.studId = :studId"),
    @NamedQuery(name = "UserCourse.findByCourseId", query = "SELECT u FROM UserCourse u WHERE u.courseId = :courseId")})
public class UserCourse implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "stud_id")
    private int studId;
    @Basic(optional = false)
    @Column(name = "course_id")
    private int courseId;

    public UserCourse() {
    }

    public UserCourse(Integer id) {
        this.id = id;
    }

    public UserCourse(Integer id, int studId, int courseId) {
        this.id = id;
        this.studId = studId;
        this.courseId = courseId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getStudId() {
        return studId;
    }

    public void setStudId(int studId) {
        this.studId = studId;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserCourse)) {
            return false;
        }
        UserCourse other = (UserCourse) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "courstud.UserCourse[ id=" + id + " ]";
    }
    
}
